lgeneral (1.4.4-4) unstable; urgency=medium

  * Team Upload
  * fix building with gettext 0.23.x (Closes: #1092229)

 -- Anatoliy Gunya <csm.sub21@gmail.com>  Thu, 09 Jan 2025 21:27:09 +0300

lgeneral (1.4.4-3) unstable; urgency=medium

  * Team Upload
  * Set Rules-Requires-Root: no

  [ Anatoliy Gunya ]
  * Fix building with newer autotools. (Closes: #1066397)

 -- Alexandre Detiste <tchet@debian.org>  Mon, 02 Dec 2024 10:48:04 +0100

lgeneral (1.4.4-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Trim trailing whitespace.
    Changes-By: lintian-brush
    Fixes: lintian: trailing-whitespace
    See-also: https://lintian.debian.org/tags/trailing-whitespace.html
  * Fix day-of-week for changelog entries 1.1.1-3, 0.5.0-3, 0.5.0-2.
    Changes-By: lintian-brush
    Fixes: lintian: debian-changelog-has-wrong-day-of-week
    See-also:
    https://lintian.debian.org/tags/debian-changelog-has-wrong-day-of-week.html

  [ undef ]
  * change encoding of lgeneral.desktop to UTF-8
    lgeneral.desktop has ISO-8859-1 encoding. It violates the specification.

  [ Markus Koschany ]
  * Declare compliance with Debian Policy 4.6.2.

 -- Markus Koschany <apo@debian.org>  Sat, 25 Feb 2023 17:55:55 +0100

lgeneral (1.4.4-1) unstable; urgency=medium

  * New upstream version 1.4.4.

 -- Markus Koschany <apo@debian.org>  Sun, 31 Jan 2021 16:54:52 +0100

lgeneral (1.4.3-2) unstable; urgency=medium

  * Switch to debhelper-compat = 13.
  * Declare compliance with Debian Policy 4.5.1.
  * Use canonical VCS URI.
  * Remove get-orig-source target.
  * Build without autoreconf and wait until upstream has time to fix build
    failures with newer gettext versions. (Closes: #978370)

 -- Markus Koschany <apo@debian.org>  Fri, 01 Jan 2021 23:09:24 +0100

lgeneral (1.4.3-1) unstable; urgency=medium

  * New upstream version.
  * Switch to compat level 11.
  * Use canonical VCS address.
  * Declare compliance with Debian Policy 4.1.2.
  * Use https for Format field and update copyright years.
  * Drop deprecated menu file and xpm icon.

 -- Markus Koschany <apo@debian.org>  Mon, 25 Dec 2017 20:10:58 +0100

lgeneral (1.3.1-1) unstable; urgency=medium

  * Imported Upstream version 1.3.1.
  * Vcs-Browser: Use https.
  * Update my e-mail address.

 -- Markus Koschany <apo@debian.org>  Wed, 13 Jan 2016 17:32:49 +0100

lgeneral (1.3.0-1) unstable; urgency=medium

  * Imported Upstream version 1.3.0.
  * Drop all patches. Merged upstream.

 -- Markus Koschany <apo@gambaru.de>  Wed, 12 Aug 2015 10:50:15 +0200

lgeneral (1.2.6-2) unstable; urgency=medium

  * Append -std=gnu89 to CFLAGS and force building with the older
    GCC standard gnu89 instead of the new default gnu11. This fixes the FTBFS
    with GCC-5. (Closes: #777933)
  * Add segfaults-when-running-headless.patch.
    lgc-pg requires a running graphical desktop environment for the conversion.
    Now lgc-pg prints an error message and exits gracefully when it does not
    detect this requirement. (Closes: #784259)
  * Add lgc-pg-man-page.patch and mention that lgc-pg requires a graphical
    desktop environment for the conversion.

 -- Markus Koschany <apo@gambaru.de>  Fri, 26 Jun 2015 20:53:26 +0200

lgeneral (1.2.6-1) unstable; urgency=medium

  * Imported Upstream version 1.2.6.
  * Do not repack the tarball anymore. It is now completely DFSG compatible.
    Remove repacksuffix option from watch file again and Files-Excluded
    paragraph from debian/copyright.
  * Drop all patches. They were merged upstream.
  * Remove override for dh_install.
  * Drop README.source. LGeneral is pristine now.
  * Remove lgeneral.png from debian/clean.

 -- Markus Koschany <apo@gambaru.de>  Tue, 17 Mar 2015 09:45:45 +0100

lgeneral (1.2.5+dfsg-1) unstable; urgency=medium

  * Imported Upstream version 1.2.5+dfsg.
  * Add repacksuffix option to debian/watch.

 -- Markus Koschany <apo@gambaru.de>  Sat, 07 Mar 2015 11:28:48 +0100

lgeneral (1.2.4+dfsg-2) unstable; urgency=medium

  * Upload to unstable.
  * Improve README.Debian and better explain how LGeneral is supposed to work.
    Thanks to Tomas Pospisek for the report. (Closes: #774361)
  * Update copyright years.

 -- Markus Koschany <apo@gambaru.de>  Thu, 12 Feb 2015 17:26:49 +0100

lgeneral (1.2.4+dfsg-1) experimental; urgency=medium

  * Imported Upstream version 1.2.4+dfsg.
  * Update debian/copyright for new release.
    Use Files-Excluded directive and remove non-free mapnames file.
  * Drop the following patches. They were merged upstream.
    - Fix-FTBFS-Hardening-Werror-format-security.patch.
    - Improve-manpages-of-LGeneral.patch.
    - conflicting-function-declarations.patch.
  * Update Fix-icon-path-and-escape-hyphens.patch and rename it
    to icon.patch. Use hicolor directory instead of /usr/share/pixmaps.
    Man page fixes were applied upstream.
  * debian/rules: Add get-orig-source target.
  * Update README.source.
  * Install desktop icon to hicolor directory.
  * Update debian/watch for 1.2.4.
  * Add remove-mapnames.patch. Do not require mapnames to build LGeneral.
  * Add clean file and ensure that LGeneral can be built twice in a row.

 -- Markus Koschany <apo@gambaru.de>  Wed, 29 Oct 2014 20:29:44 +0100

lgeneral (1.2.3+dfsg-3) unstable; urgency=medium

  * Tighten dependency on lgc-pg because it also contains the necessary sound
    files to make the game work. Thanks to Roland Hieber for the report.
    (Closes: #764668)
  * Update VCS-Browser field to new canonical address.

 -- Markus Koschany <apo@gambaru.de>  Fri, 10 Oct 2014 12:25:26 +0200

lgeneral (1.2.3+dfsg-2) unstable; urgency=medium

  * Add conflicting-function-declarations.patch.
    - Fix possible undefined compiler behaviour. Thanks to Michael Tautschnig
      for the report and patch. (Closes: #749161)
  * Update copyright years.
  * debian/control:
    - Declare compliance with Debian policy 3.9.6.
    - Only suggest game-data-packager since LGeneral is going to be installed
      into the main archive.
    - Change Section to games.
      A free data package, lgeneral-data, is now available hence the game can
      be moved to main. (Closes: #193061)
    - Move lgc-pg to Recommends because it is not strictly required to run the
      game. It is still very useful for development purposes.
    - Depend on lgeneral-data.
    - Update package description of the lgeneral binary package.
  * lgeneral.menu: Update longtitle.
  * debian/rules:
    - Replace dh_auto_install override with dh_install override.
    - Do not export DH_OPTIONS. It seems to make no difference.
  * Drop source/include-binaries because it is not necessary for the xpm file.
  * Fix lintian warning wrong weekday.
  * Update README.Debian. Mention new free data package.

 -- Markus Koschany <apo@gambaru.de>  Tue, 30 Sep 2014 11:30:10 +0200

lgeneral (1.2.3+dfsg-1) unstable; urgency=low

  * New Maintainer. (Closes: #465942)
  * New upstream release.
  * Reintroduction of lgc-pg which is now part of the sources.
  * Depend on game-data-packager and lgc-pg to provide game data
    for users who own a copy of Panzer General. For more information please
    refer to README.Debian. (Closes: #690683)
  * Switch to package format 3.0 (quilt).
  * Build with hardening=+all.
  * Set compat level to 9 and debhelper build-dep to >=9 for automatic
    hardening build flags.
  * Bump Standards-Version to 3.9.4.
  * Fix FTBFS because of Hardening Werror=format-security error.
  * Remove and replace non-free files and make LGeneral comply to the DFSG.
    (Closes: #691451)
  * Improve and update LGeneral's manpages.
  * Update debian/copyright to copyright format 1.0.
  * Add icon to Debian menu.

 -- Markus Koschany <apo@gambaru.de>  Thu, 25 Oct 2012 17:56:28 +0200

lgeneral (1.1.1-5) unstable; urgency=low

  * QA upload.
    + Set maintainer to Debian QA Group <packages@qa.debian.org>
  * Add Homepage.
  * Add ${misc:Depends} to binary package.
  * Remove broken URL from manpage. (Closes: #320730).
  * Add desktop file from Ubuntu. (Remove deprecated Encoding).
    + Install in rules and call dh_desktop.
  * Make clean not ignore errors.
  * Add watch file.
  * Version debhelper build-dep and set to >= 5.0.0.
  * Move DH_COMPAT from rules to debian/compat and set to 5.
  * Bump Standards Version to 3.8.0.

 -- Barry deFreese <bdefreese@debian.org>  Wed, 04 Feb 2009 13:38:51 -0500

lgeneral (1.1.1-4) unstable; urgency=low

  * moved to contrib (closes: bug#239673)

 -- Lukasz Jachowicz <honey@debian.org>  Wed, 28 Jul 2004 10:34:02 +0200

lgeneral (1.1.1-3) unstable; urgency=low

  * Fixed debian/rules to update some timestamps (closes: bug#214296)
  * Do not suggest lgeneral-data anymore - this package woud be
    DFSG-compilant.

 -- Lukasz Jachowicz <honey@debian.org>  Mon, 02 Feb 2004 02:05:01 +0200

lgeneral (1.1.1-2) unstable; urgency=low

  * Now suggests lgeneral-data and lgc-pg (closes: bug#193061)

 -- Lukasz Jachowicz <honey@debian.org>  Thu,  2 Oct 2003 19:27:11 +0000

lgeneral (1.1.1-1) unstable; urgency=low

  * New upstream release (closes: bug#149008)

 -- Lukasz Jachowicz <honey@debian.org>  Tue,  8 Apr 2003 06:43:13 +0000

lgeneral (0.5.0-4) unstable; urgency=low

  * Fixed typo in description and manual (closes: bug#122127)

 -- Lukasz Jachowicz <honey@debian.org>  Mon, 3 Dec 2001 15:12:23 +0200

lgeneral (0.5.0-3) unstable; urgency=low

  * Updated the libSDL dependencies (closes: bug#119287)
  * Fixed typo in description (closes: bug#119662)
  * Fixed build dependencies (closes: bug#117935)

 -- Lukasz Jachowicz <honey@debian.org>  Mon, 12 Nov 2001 17:15:33 +0200

lgeneral (0.5.0-2) unstable; urgency=low

  * Fixed missing dependencies (closes: bug#114683)

 -- Lukasz Jachowicz <honey@debian.org>  Mon, 08 Oct 2001 23:25:35 +0200

lgeneral (0.5.0-1) unstable; urgency=low

  * Initial Release.
  * Adjusted the configure.in file to fix path problems.
  * Wrote the manual page for lgeneral.
  * Renamed ChangeLog to changelog.

 -- Lukasz Jachowicz <honey@debian.org>  Thu, 20 Sep 2001 17:05:55 +0200
